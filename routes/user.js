const express = require("express");
const router = express.Router();
const bcrypt = require("bcryptjs");
const user = require("../db/models/user");
const { Sequelize } = require("sequelize");

router.use(express.json());

router.get("/getAllUser", async (req, res) => {
  try {
    const getAllUsers = await user.findAll();
    console.log(getAllUsers);
    res.status(200).json({
      status: "success",
      data: getAllUsers,
    });
  } catch (err) {
    console.error(err.message);
  }
})

router.post("/createUser", async (req, res) => {
  try {
    const checkIfExists = await user.count({
      where: {
        email: req.body.email,
      }
    })
    console.log(checkIfExists)
    if (checkIfExists == 0) {
      const new_user = await user.create({
        email: req.body.email
      });
      res.status(201).json({
        status: "success",
        data: {
          email: new_user.dataValues.email,
          stocks: new_user.dataValues.stocks,
        },
      });
    } else {
      res.status(200).json({
        data: "User already exists."
      })
    }
  } catch (err) {
    // console.log(err);
  }

});

router.post("/getUser", async (req, res) => {
  try {
    const getUser = await user.findOne({
      where: {
        email: req.body.email
      },
      raw: true,
    });
    console.log(getUser);
    res.status(200).json({
      status: "success",
      data: getUser,
    });
  } catch (err) {
    console.error(err.message);
  }
});

router.post("/addStock", async (req, res) => {
  try {
    await user.update(
      {
        stocks: Sequelize.fn(
          "array_append",
          Sequelize.col("stocks"),
          req.body.stock_to_be_added
        ),
      },
      {
        where: { email: req.body.email },
      }
    );
    // console.log(req.body);
    res.status(200).json({
      status: "success",
      data: {
        stocks: req.body.stocks,
        email: req.body.email,
      },
    });
  } catch (err) {
    // console.log(err);
  }
});

router.post("/removeStock", async (req, res) => {
  try {
    await user.update(
      {
        stocks: Sequelize.fn(
          "array_remove",
          Sequelize.col("stocks"),
          req.body.stock_to_be_removed
        ),
      },
      {
        where: { email: req.body.email },
      }
    );
    res.status(200).json({
      status: "success",
      data: {
        stocks: req.body.stocks,
      },
    });
  } catch (err) {
    // console.log(err);
  }
});


module.exports = router;