import React, { useEffect, useContext, useState } from "react";
import { useNavigate } from "react-router-dom";
import stockwatch from '../images/stockwatch.png';
import search from '../images/search.png';
import aapl from '../images/aapl.png';
import twtr from '../images/twtr.png';
import amzn from '../images/amzn.png';
import googl from '../images/googl.png';
import fb from '../images/fb.png';
import ibm from '../images/ibm.png';
import coin from '../images/coin.png';
import gme from '../images/gme.png';
import pypl from '../images/pypl.png';
import ma from '../images/ma.png';
import './styles/components.css'
import Axios from 'axios';


export function Nav() {
    const [searchInput, setSearchInput] = useState("");
    const [company, setCompany] = useState({

    });

    let navigate = useNavigate();

    function handleChange(e) {
        e.preventDefault();
        setSearchInput(e.target.value);
        if (e.key === 'Enter') {
            handleSubmit();
        }
    }

    function handleSubmit(e) {
        console.log(searchInput)
        if(searchInput != ""){
        navigate("/stocks/" + searchInput.toUpperCase());
        }
    }

    function handleBtnClick(e) {
        navigate("/symbol/" + e.toUpperCase());
        window.location.reload()
    }

    function goToGeneral(e) {
        navigate("/news/general");
    }
    function goToCrypto(e) {
        navigate("/news/crypto");
    }
    function goToForex(e) {
        navigate("/news/forex");
    }
    function goToMerger(e) {
        navigate("/news/merger");
    }
    function goToFav(e) {
        navigate("/favorites");
    }

    return (
        <nav>
            <span className="nav-top">
                <form action="/">
                    <button className="nav-button">
                        <img src={stockwatch} className='stockwatch-logo' />
                    </button>
                </form>
                <span className="right">
                    <button id="about-btn" onClick={goToGeneral}>
                        General
                    </button>
                    {/* <button id="about-btn" onClick={goToCrypto}>
                        Crypto
                    </button> */}
                    <button id="about-btn" onClick={goToForex}>
                        Forex
                    </button>
                    <button id="about-btn" onClick={goToMerger}>
                        Merger
                    </button>
                    <button id="about-btn" onClick={goToFav}>
                        Favorites
                    </button>
                    <form id="search-container" onSubmit={handleSubmit}>
                        <input type="text" placeholder="Search Symbol" className="search-input" onChange={handleChange} />
                        <button>
                            <input type="image" src={search} className="search-img" alt="search icon" />
                        </button>
                    </form>
                </span>

            </span>

            <div className="hwrap">
                <div className="hmove">
                    <div className="hitem">
                        <button className="stock-btn" onClick={() => handleBtnClick('amzn')}><img className="stock-logo" src={amzn} />AMAZON (AMZN)</button>
                        <button className="stock-btn" onClick={() => handleBtnClick('googl')}><img className="stock-logo" src={googl} />ALPHABET (GOOGL)</button>
                        <button className="stock-btn" onClick={() => handleBtnClick('aapl')}><img className="stock-logo" src={aapl} />APPLE (AAPL)</button>
                        <button className="stock-btn" onClick={() => handleBtnClick('meta')}><img className="stock-logo" src={fb} />META (META)</button>
                        <button className="stock-btn" onClick={() => handleBtnClick('twtr')}><img className="stock-logo" src={twtr} />TWITTER (TWTR)</button>
                        <button className="stock-btn" onClick={() => handleBtnClick('ibm')}><img className="stock-logo" src={ibm} />IBM (IBM)</button>
                        <button className="stock-btn" onClick={() => handleBtnClick('coin')}><img className="stock-logo" src={coin} />COINBASE (COIN)</button>
                        <button className="stock-btn" onClick={() => handleBtnClick('gme')}><img className="stock-logo" src={gme} />GAMESTOP (GME)</button>
                        <button className="stock-btn" onClick={() => handleBtnClick('pypl')}><img className="stock-logo" src={pypl} />PAYPAL (PYPL)</button>
                        <button className="stock-btn" onClick={() => handleBtnClick('ma')}><img className="stock-logo" src={ma} />MASTERCARD (MA)</button>
                    </div>
                </div>
            </div>
        </nav >
    );
}