import React, { useEffect, useState } from 'react';
import Axios from 'axios';
import logo from "./logo.svg";
import { Home } from "./routes/Home";
import { About } from "./routes/About";
import { CompanyPage } from './routes/CompanyPage';
import { Crypto } from './routes/Crypto';
import { Merger } from './routes/Merger';
import { Forex } from './routes/Forex';
import { General } from './routes/General';
import { FindStock } from './routes/FindStock';
import { Favorites } from './routes/Favorites';
import {
  BrowserRouter,
  Routes,
  Route,
  Link
} from "react-router-dom";
import "./App.css";
import ReactGA from 'react-ga';
import jwt_decode from 'jwt-decode';
import { GoogleOAuthProvider } from '@react-oauth/google';

function App() {
  const TRACKING_ID = 'UA-235622015-1';
  ReactGA.initialize(TRACKING_ID, {
    siteSpeedSampleRate: 100
  });
  useEffect(() => {
    ReactGA.pageview(window.location.pathname + window.location.search);
  }, []);

  return (
    <GoogleOAuthProvider clientId="177455138498-17vsg945r11hdil78p5kl1kvpvs66vlj.apps.googleusercontent.com">
      <div className="App">
        <BrowserRouter>
          <Routes>
            <Route exact path="/" element={<About />} />
            <Route path="/symbol/:symbol" element={<CompanyPage />} />
            <Route exact path="/about" element={<About />} />
            <Route exact path="/news/general" element={<General />} />
            <Route exact path="/news/crypto" element={<Crypto />} />
            <Route exact path="/news/forex" element={<Forex />} />
            <Route exact path="/news/merger" element={<Merger />} />
            <Route exact path="/stocks/:symbol" element={<FindStock />} />
            <Route exact path="/favorites" element={<Favorites />} />
          </Routes>
        </BrowserRouter>
      </div >
    </GoogleOAuthProvider>
  );
}

export default App;