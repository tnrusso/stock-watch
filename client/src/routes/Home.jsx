import React, { useEffect, useContext, useState } from "react";
import { useNavigate } from "react-router-dom";
import { Nav } from '../components/Nav';
import { Footer } from '../components/Footer';
import './styles/routes.css'
import Axios from 'axios';
import stocks from '../images/stocks.jpg'

export function Home() {
    const [symbols, setSymbols] = useState([]);
    const [news, setNews] = useState([{
    }]);
    let navigate = useNavigate();

    useEffect(() => {
        Axios({
            method: "GET",
            url: "https://stocks-watch.herokuapp.com/marketNews",
            headers: {
                "Content-Type": "application/json"
            }
        }).then(res => {
            // console.log(res.data);
            setNews(res.data);
        });
    }, []);

    function timeConverter(UNIX_timestamp) {
        var a = new Date(UNIX_timestamp * 1000);
        return a.toLocaleString();
    }

    function openArticle(e) {
        // console.log(e);
        window.open(e, '_blank').focus();
    }

    return (
        <>
            <Nav />
            {/* <form action="../../../post" method="post"
                className="form">
                <button type="submit">Connected?</button>
            </form>
            <button onClick={testRequest}>click</button> */}
            <div className="articles-container">
                <h1 className="custom-header center-text">Market News</h1>
                {news.slice(0, 50).map((newsArticle) => {
                    if (newsArticle.source !== "Bloomberg") {
                        return (
                            <button className="article-box" value={newsArticle.url} onClick={() => openArticle(newsArticle.url)}>
                                <article className="ar">
                                    <p className="article-source">{newsArticle.source}</p>
                                    <p className="article-headline">{newsArticle.headline}</p>
                                    <br></br>
                                    <p className="article-time">{timeConverter(newsArticle.datetime)}</p>
                                    <br></br>
                                    <p className="article-summary"><img src={newsArticle.image} className='article-image' />{newsArticle.summary}</p>
                                    <p></p>
                                </article>
                            </button>
                        );
                    }
                })}
            </div>
        </ >
    );
}