import React, { useEffect, useContext, useState } from "react";
import { useNavigate } from "react-router-dom";
import { Nav } from '../components/Nav';
import { Footer } from '../components/Footer';
import './styles/routes.css'
import jwt_decode from 'jwt-decode';
import { GoogleLogin } from '@react-oauth/google';
import UserAPI from "../apis/UserAPI";

export function Favorites() {
    const [user, setUser] = useState({})
    const [stocks, setStocks] = useState([]);

    let navigate = useNavigate();

    async function getUserData(user_email) {
        if (window.localStorage.getItem("isLoggedIn") && window.localStorage.getItem("email")) {
            try {
                const user_favorites = await UserAPI.post("/getUser", {
                    email: user_email || window.localStorage.getItem("email"),
                })
                setStocks(user_favorites.data.data.stocks)
            } catch (e) {
                console.error(e)
            }
        }
    }

    useEffect(() => {
        getUserData();
    }, [])

    function goToCompanyPage(sym) {
        navigate("/symbol/" + sym.toUpperCase());
        window.location.reload()
    }

    async function handleCallbackResponse(response) {
        var userObject = jwt_decode(response.credential);
        setUser(userObject);
        window.localStorage.setItem("isLoggedIn", true);
        window.localStorage.setItem("email", userObject.email)
        try {
            const response = await UserAPI.post("/createUser", {
                email: userObject.email
            })
        } catch (e) {
            console.error(e);
        }
        getUserData(userObject.email);
    }

    function handleSignOut(e) {
        setUser({});
        window.localStorage.removeItem("isLoggedIn")
        window.localStorage.removeItem("email")
    }

    return (
        <>
            <Nav />
            <div id="favorites-container">
                {
                    (window.localStorage.getItem("isLoggedIn") || Object.keys(user).length != 0)
                        ?
                        <div>
                            {(Object.keys(stocks).length != 0 != 0)
                                ?
                                <>
                                    <div className="favorites-list-container">
                                        <h1>Favorites</h1>
                                        <ul className="favorites-list">
                                            {stocks.map((stock) => {
                                                return (
                                                    <li className="favorites-stock-item">
                                                        <button className="stocks-btn" value={stock} onClick={() => goToCompanyPage(stock)}>
                                                            {stock}
                                                        </button>
                                                    </li>
                                                );

                                            })}
                                        </ul>
                                    </div>
                                </>
                                :
                                <h3>No stocks saved. You can quickly view stocks that you favorite here.</h3>
                            }
                            <div className="signout-btn-container">
                                < button className="google-logout-btn" onClick={(e) => handleSignOut(e)}>Sign Out</button>
                            </div>
                        </div>
                        :
                        <div className="sign-in-container">
                            <h2>Sign in with Google to view favorite stocks</h2>
                            <GoogleLogin
                                onSuccess={credentialResponse => {
                                    handleCallbackResponse(credentialResponse)
                                }}
                                onError={() => {
                                    console.log('Login Failed');
                                }}
                            />
                        </div>
                }
            </div>
        </ >
    );
}