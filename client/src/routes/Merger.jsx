import React, { useEffect, useContext, useState } from "react";
import { useNavigate } from "react-router-dom";
import { Nav } from '../components/Nav';
import { Footer } from '../components/Footer';
import './styles/routes.css'
import Axios from 'axios';
import stocks from '../images/stocks.jpg'


export function Merger() {
    const [news, setNews] = useState([{
    }]);
    let navigate = useNavigate();

    useEffect(() => {
        Axios({
            method: "GET",
            url: "https://stocks-watch.herokuapp.com/categoryMarketNews",
            params: { "category": "merger" },
            headers: {
                "Content-Type": "application/json"
            }
        }).then(res => {
            // console.log(res.data);
            setNews(res.data);
        });
    }, []);

    function timeConverter(UNIX_timestamp) {
        var a = new Date(UNIX_timestamp * 1000);
        return a.toLocaleString();
    }

    function openArticle(e) {
        // console.log(e);
        window.open(e, '_blank').focus();
    }

    return (
        <>
            <Nav />
            <div className="articles-container">
                <h1 className="custom-header center-text">Merger Market News</h1>
                {news.slice(0, 10).map((newsArticle) => {
                    return (
                        <button className="article-box" value={newsArticle.url} onClick={() => openArticle(newsArticle.url)}>
                            <article className="ar">
                                <p className="article-source">{newsArticle.source}</p>
                                <p className="article-headline">{newsArticle.headline}</p>
                                <br></br>
                                <p className="article-time">{timeConverter(newsArticle.datetime)}</p>
                                <br></br>
                                <p className="article-summary"><img src={newsArticle.image} className='article-image' />{newsArticle.summary}</p>
                                <p></p>
                            </article>
                        </button>
                    );
                })}
            </div>
        </ >
    );
}