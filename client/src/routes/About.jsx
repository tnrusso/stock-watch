import React, { useEffect, useContext, useState } from "react";
import { useNavigate } from "react-router-dom";
import { Nav } from '../components/Nav';
import finnhub from '../images/finnhub.png'
import stockwatch from '../images/stockwatch.png'

export function About() {

    return (
        <>
            <Nav />
            <span className="about-desc">
                <img src={stockwatch} className="stockwatch-about-page-img"></img>
                <br></br>
                <p>
                    <span className="stockwatch-color">Stock Watch</span> allows for users search for up-to-the-minute stock market information and also get the latest stock market news.
                    <br></br><br></br>
                    Simply search for a stock by typing the symbol in the search box to view information about that stock, including:
                    <br></br>
                </p>
                <ul>
                    <li>Current price</li>
                    <li>Price changes</li>
                    <li>Price highs and lows</li>
                    <li>Dividends</li>
                    <li>Company quarterly earnings</li>
                    <li>Company market news</li>
                    <li>And more!</li>
                </ul>
                <span><span className="stockwatch-color">Stock Watch</span> gets up-to-the-minute data from the <span className="finnhub-color">Finnhub Stock API</span> <span className="finnhub-color-free">(Free plan)</span>.</span>
                <img src={finnhub} className="finnhub-about-page-img"></img>
                <span>
                    Website: <a href="https://finnhub.io/">https://finnhub.io/</a>
                    <br></br>
                    {/* Documentation: <a href="https://finnhub.io/docs/api">https://finnhub.io/docs/api</a> */}
                </span>
                <br></br><br></br><br></br>
            </span>

        </>
    );
}