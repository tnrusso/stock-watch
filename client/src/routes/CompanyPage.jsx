import React, { useEffect, useContext, useState } from "react";
import { Nav } from '../components/Nav';
import { Footer } from '../components/Footer';
import {
    BrowserRouter,
    Routes,
    Route,
    Link,
    useParams
} from "react-router-dom";
import './styles/routes.css'
import Axios from 'axios';
import { Line } from "react-chartjs-2";
import Chart from 'chart.js/auto'
import stocks from '../images/stocks.jpg'
import UserAPI from "../apis/UserAPI";

export function CompanyPage() {
    const [favorited, setFavorited] = useState(false);
    const [earnings, setEarnings] = useState([]);
    const [actual, setActual] = useState([]);
    const [estimate, setEstimate] = useState([]);
    const [period, setPeriod] = useState([]);
    const [companyData, setCompanyData] = useState({});
    const [companyNews, setCompanyNews] = useState([{}]);
    const [basicFinancials, setBasicFinancials] = useState([{}]);
    const [symbolData, setSymbolData] = useState([])
    const [dividends, setDividends] = useState([
        {
            "exDate": "N/A",
            "amount": "N/A"
        },
        {
            "exDate": "N/A",
            "amount": "N/A"
        },
        {
            "exDate": "N/A",
            "amount": "N/A"
        },
        {
            "exDate": "N/A",
            "amount": "N/A"
        },
        {
            "exDate": "N/A",
            "amount": "N/A"
        },
        {
            "exDate": "N/A",
            "amount": "N/A"
        },
    ]);
    const [quote, setQuote] = useState({
        'c': 0,
        'd': 0,
        'dp': 0,
        'h': 0,
        'l': 0,
        'o': 0,
        'pc': 0
    })
    const [data, setData] = useState({
        labels: [],
        datasets: [
            {
                label: "Actual",
                data: [],
                fill: true,
                backgroundColor: "rgba(75,192,192,0.2)",
                borderColor: "rgba(75,192,192,1)"
            },
            {
                label: "Estimate",
                data: [],
                fill: false,
                borderColor: "#742774"
            }
        ]
    });

    const [candle, setCandle] = useState({
        labels: [],
        datasets: [
            {
                label: "Open",
                data: [],
                fill: true,
                backgroundColor: "rgb(205, 205, 255,0.2)",
                borderColor: "rgb(0, 0, 255,1)"
            },
            {
                label: "Closed",
                data: [],
                fill: true,
                backgroundColor: "rgb(255, 205, 205,0.2)",
                borderColor: "rgb(255, 0, 0, 1)"
            },
            {
                label: "High",
                data: [],
                fill: true,
                backgroundColor: "rgb(150, 255, 136, 0.2)",
                borderColor: "rgb(30, 255, 0, 1)"
            },
            {
                label: "Low",
                data: [],
                fill: true,
                backgroundColor: "rgb(226, 245, 158, 0.2)",
                borderColor: "rgb(200, 255, 0, 1)"
            }
        ]
    });

    const { symbol } = useParams();

    async function getUserData(user_email) {
        if (window.localStorage.getItem("isLoggedIn") && window.localStorage.getItem("email")) {
            try {
                const user_favorites = await UserAPI.post("/getUser", {
                    email: user_email || window.localStorage.getItem("email"),
                })
                let user_stocks = user_favorites.data.data.stocks
                setFavorited(user_stocks.includes(symbol))
            } catch (e) {
                console.error(e)
            }
        }
    }

    useEffect(() => {
        getUserData();
    }, [])

    async function onClickFavorite(sym) {
        console.log(sym)
        try {
            const add_to_favorites = await UserAPI.post("/addStock", {
                email: window.localStorage.getItem("email"),
                stock_to_be_added: sym
            })
            // console.log(add_to_favorites)
            setFavorited(true);
        } catch (e) {
            console.error(e)
        }
    }

    async function onClickUnfavorite(sym) {
        // console.log(sym)
        try {
            const remove_from_favorites = await UserAPI.post("/removeStock", {
                email: window.localStorage.getItem("email"),
                stock_to_be_removed: sym
            })
            // console.log(remove_from_favorites)
            setFavorited(false);
        } catch (e) {
            console.error(e)
        }
    }


    useEffect(() => {
        Axios({
            method: "GET",
            url: "https://stocks-watch.herokuapp.com/getEarnings",
            params: { "symbol": symbol },
            headers: {
                "Content-Type": "application/json"
            }
        }).then(res => {
            //            console.log(res.data);
            setEarnings(res.data)
            let act = []
            let est = []
            let per = []
            for (let i = 0; i < res.data.length; i++) {
                act.push(res.data[i].actual);
                est.push(res.data[i].estimate);
                let ss = res.data[i].period.indexOf("T");
                per.push(res.data[i].period.substring(0, ss));
            }
            setActual(act);
            setEstimate(est)
            setPeriod(per);
            setData({
                labels: per,
                datasets: [
                    {
                        label: "Actual",
                        data: act,
                        fill: true,
                        backgroundColor: "rgb(205, 205, 255,0.2)",
                        borderColor: "rgb(0, 0, 255,1)"
                    },
                    {
                        label: "Estimate",
                        data: est,
                        fill: true,
                        backgroundColor: "rgb(255, 205, 205,0.2)",
                        borderColor: "rgb(255, 0, 0, 1)"
                    }
                ]
            })
        });
        Axios({
            method: "GET",
            url: "https://stocks-watch.herokuapp.com/quote",
            params: { "symbol": symbol },
            headers: {
                "Content-Type": "application/json"
            }
        }).then(res => {
            //console.log(res);
            setQuote(res.data);
        });

        Axios({
            method: "GET",
            url: "https://stocks-watch.herokuapp.com/candle",
            params: { "symbol": symbol },
            headers: {
                "Content-Type": "application/json"
            }
        }).then(res => {
            //console.log(res);
            let temp_dates = []
            for (let i = 0; i < res.data.o.length; i++) {
                temp_dates.push(timeConverter(res.data.t[i]));
            }
            setCandle({
                labels: temp_dates,
                datasets: [
                    {
                        label: "Open",
                        data: res.data.o,
                        fill: true,
                        backgroundColor: "rgb(205, 205, 255,0.2)",
                        borderColor: "rgb(0, 0, 255,1)"
                    },
                    {
                        label: "Closed",
                        data: res.data.c,
                        fill: true,
                        backgroundColor: "rgb(255, 205, 205,0.2)",
                        borderColor: "rgb(255, 0, 0, 1)"
                    },
                    {
                        label: "High",
                        data: res.data.h,
                        fill: true,
                        backgroundColor: "rgb(150, 255, 136, 0.2)",
                        borderColor: "rgb(30, 255, 0, 1)"
                    },
                    {
                        label: "Low",
                        data: res.data.l,
                        fill: true,
                        backgroundColor: "rgb(226, 245, 158, 0.2)",
                        borderColor: "rgb(200, 255, 0, 1)"
                    }
                ]
            });
        });

        Axios({
            method: "GET",
            url: "https://stocks-watch.herokuapp.com/companyProfile",
            params: { "symbol": symbol },
            headers: {
                "Content-Type": "application/json"
            }
        }).then(res => {
            // console.log(res);
            setCompanyData(res.data);
        });

        Axios({
            method: "GET",
            url: "https://stocks-watch.herokuapp.com/companyProfile2",
            params: { "symbol": symbol },
            headers: {
                "Content-Type": "application/json"
            }
        }).then(res => {
            // console.log(res.data);
            setSymbolData(res.data);
        });

        var todayDate = new Date().toISOString().slice(0, 10);
        var minus10 = new Date();
        minus10.setDate(minus10.getDate() - 5);
        minus10 = minus10.toISOString().slice(0, 10);

        Axios({
            method: "GET",
            url: "https://stocks-watch.herokuapp.com/companyNews",
            params: {
                "symbol": symbol,
                "minus10Days": minus10,
                "today": todayDate
            },
            headers: {
                "Content-Type": "application/json"
            }
        }).then(res => {
            //console.log(res);
            setCompanyNews(res.data);
        });

        Axios({
            method: "GET",
            url: "https://stocks-watch.herokuapp.com/dividends",
            params: {
                "symbol": symbol
            },
            headers: {
                "Content-Type": "application/json"
            }
        }).then(res => {
            // console.log(res);
            // if (res.data.data.length > 0) {
            //     setDividends(res.data.data.reverse());
            // }
        });

        Axios({
            method: "GET",
            url: "https://stocks-watch.herokuapp.com/basicFinancials",
            params: {
                "symbol": symbol
            },
            headers: {
                "Content-Type": "application/json"
            }
        }).then(res => {
            // console.log(res.data['metric']['52WeekHigh']);
            setBasicFinancials(res.data['metric']);
        });

    }, [])

    function timeConverter(UNIX_timestamp) {
        var a = new Date(UNIX_timestamp * 1000);
        return a.toLocaleString();
    }

    function getDateAndTime(dt) {
        if (dt == "N/A") {
            return "N/A";
        }
        let ss = dt.indexOf("T");
        let d = dt.substring(0, ss);
        return d;
    }

    function openArticle(e) {
        //console.log(e);
        window.open(e, '_blank').focus();
    }

    return (
        <>
            <Nav />
            <div>

                <div className="company-desc">
                    <h1>{symbolData.name}{' '}({symbolData.ticker})</h1>
                    {
                        (window.localStorage.getItem("isLoggedIn") && !favorited)
                        && <button className="favorite-btn" onClick={(e) => onClickFavorite(symbol)}>Favorite ✓</button>
                    }
                    {
                        (window.localStorage.getItem("isLoggedIn") && favorited)
                        && <button className="unfavorite-btn" onClick={(e) => onClickUnfavorite(symbol)}>Unfavorite x</button>
                    }
                </div>
                <div id="chart-container">
                    <div className="chart-section">
                        <h3>Open/Closed/High/Low</h3>
                        <Line
                            datasetIdKey="id2"
                            data={candle}
                            fallbackContent='candle'
                            id='line-graph'
                        />
                    </div>
                    <div className="chart-section">
                        <h3>Company Earnings</h3>
                        <Line
                            datasetIdKey="id"
                            data={data}
                            fallbackContent='company earnings'
                            id='line-graph'
                        />
                    </div>
                </div>

                <div className="table-section">
                    <table className="company-table">
                        <tbody>
                            <tr>
                                <td className="td-1">Current Price</td>
                                <td>{quote.c}</td>
                            </tr>
                            <tr>
                                <td className="td-1">Change</td>
                                <td>{quote.d}</td>
                            </tr>
                            <tr>
                                <td className="td-1">Percent Change</td>
                                <td>{quote.dp}</td>
                            </tr>
                            <tr>
                                <td className="td-1">High Price (Today)</td>
                                <td>{quote.h}</td>
                            </tr>
                            <tr>
                                <td className="td-1">Low Price (Today)</td>
                                <td>{quote.l}</td>
                            </tr>
                            <tr>
                                <td className="td-1">Open Price</td>
                                <td>{quote.o}</td>
                            </tr>
                            <tr>
                                <td className="td-1">Previous Closing Price</td>
                                <td>{quote.pc}</td>
                            </tr>
                            <tr>
                                <td className="td-1">52 Week High</td>
                                <td>{basicFinancials['52WeekHigh']}</td>
                            </tr>
                            <tr>
                                <td className="td-1">52 Week Low</td>
                                <td>{basicFinancials['52WeekLow']}</td>
                            </tr>
                        </tbody>
                    </table>
                    <table className="company-table">
                        <tbody>
                            <tr>
                                <td className="td-1">Industry</td>
                                <td>{symbolData.finnhubIndustry}</td>
                            </tr>
                            <tr>
                                <td className="td-1">Country</td>
                                <td>{symbolData.country}</td>
                            </tr>
                            <tr>
                                <td className="td-1">Currency</td>
                                <td>{symbolData.currency}</td>
                            </tr>
                            <tr>
                                <td className="td-1">Exchange</td>
                                <td>{symbolData.exchange}</td>
                            </tr>
                            <tr>
                                <td className="td-1">Symbol</td>
                                <td>{symbolData.ticker}</td>
                            </tr>
                            <tr>
                                <td className="td-1">Website</td>
                                <td>{symbolData.weburl}</td>
                            </tr>
                            <tr>
                                <td className="td-1">Market Capitalization</td>
                                <td>{symbolData.marketCapitalization}</td>
                            </tr>
                            <tr>
                                <td className="td-1">Share Outstanding</td>
                                <td>{symbolData.shareOutstanding}</td>
                            </tr>
                        </tbody>
                    </table>
                    <table className="company-table">
                        <tbody>
                            <tr>
                                <th colSpan="2">Dividends</th>
                            </tr>
                            <tr>
                                <th>EX-Date</th>
                                <th>Amount</th>
                            </tr>
                            {dividends.slice(0, 6).map((dividend) => {
                                return (
                                    <tr>
                                        <td className="center-text">{getDateAndTime(dividend.exDate)}</td>
                                        <td className="center-text">{dividend.amount}</td>
                                    </tr>
                                );
                            })}
                        </tbody>
                    </table>
                </div>
                <div className="articles-container">
                    <h1>{symbolData.name} News</h1>
                    {companyNews.slice(0, 6).map((newsArticle) => {
                        return (
                            <button className="article-box" value={newsArticle.url} onClick={() => openArticle(newsArticle.url)}>
                                <article className="ar">
                                    <p className="article-source">{newsArticle.source}</p>
                                    <p className="article-headline">{newsArticle.headline}</p>
                                    <p className="article-time">{timeConverter(newsArticle.datetime)}</p>
                                    <br></br>
                                    <p className="article-summary"><img src={newsArticle.image} className='article-image' />{newsArticle.summary}</p>
                                    <p></p>
                                </article>
                            </button>
                        );
                    })}
                </div>
            </div>
        </ >
    );
}