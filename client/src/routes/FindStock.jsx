import React, { useEffect, useContext, useState } from "react";
import {
    BrowserRouter,
    Routes,
    Route,
    Link,
    useParams,
    useNavigate
} from "react-router-dom";
import { Nav } from '../components/Nav';
import Axios from 'axios';
import finnhub from '../images/finnhub.png'
import stockwatch from '../images/stockwatch.png'

export function FindStock() {
    const [stocks, setStocks] = useState([{ 'symbol': '' }])

    let navigate = useNavigate();
    const { symbol } = useParams();

    useEffect(() => {
        Axios({
            method: "GET",
            url: "https://stocks-watch.herokuapp.com/stocksList",
            params: {
                "symbol": symbol
            },
            headers: {
                "Content-Type": "application/json"
            }
        }).then(res => {
            // console.log(res.data.result);
            setStocks(res.data.result)
        });
    }, []);

    function goToCompanyPage(sym) {
        navigate("/symbol/" + sym.toUpperCase());
        window.location.reload()
    }
    // console.log(stocks)

    return (
        <>
            <Nav />
            <h1>"{symbol}" Results</h1>
            <div className="stocks-list-container">

                <ul className="stock-list">
                    {stocks.map((stock) => {

                        if (!stock.symbol.includes(".")) {
                            return (
                                <li className="stock-item">
                                    <button className="stocks-btn" value={stock.symbol} onClick={() => goToCompanyPage(stock.symbol)}>
                                        <b>{stock.description}</b>
                                        <br></br>
                                        {stock.symbol}
                                    </button>
                                </li>
                            );
                        }
                    })}
                </ul>
            </div>
        </>
    );
}