const { UUID } = require("sequelize");
const { Sequelize, DataTypes } = require("sequelize");
const db = require("../index");

const user = db.define(
  "user",
  {
    user_id: {
      type: UUID,
      defaultValue: Sequelize.UUIDV4,
      allowNull: false,
      primaryKey: true,
    },
    email: {
      type: DataTypes.STRING,
      allowNull: false,
      unique: true

    },
    stocks: {
      type: DataTypes.ARRAY(DataTypes.STRING),
      defaultValue: [],
    },
  }
);

module.exports = user;