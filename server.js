const cors = require("cors");
const bodyParser = require("body-parser");
const express = require("express");
const app = express();
const path = require("path");
const axios = require('axios');
const https = require('https')
const finnhub = require('finnhub');
require("dotenv").config();

var corsOptions = {
    origin: "http://localhost:3000",
    credentials: true,
    optionSuccessStatus: 200,
};

app.use(cors(corsOptions));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));


const api_key = finnhub.ApiClient.instance.authentications['api_key'];
api_key.apiKey = process.env.FINNHUB_API_KEY
const finnhubClient = new finnhub.DefaultApi()

// finnhubClient.symbolSearch('AAPL', (error, data, response) => {
//     console.log(data)
// });
/////

if (process.env.NODE_ENV === "production") {
    // server static content
    // npm run build
    app.use(express.static(path.join(__dirname, "client/build")));
} else {
    console.log(process.env.TEST)
}

app.post("/post", (req, res) => {
    console.log("Connected to React");
    res.redirect("/");
});

app.get("/getEarnings", (req, res) => {
    console.log(req.query.symbol);
    finnhubClient.companyEarnings(req.query.symbol, { 'limit': 40 }, (error, data, response) => {
        // console.log(data)
        res.send(data);
    });
})

app.get("/quote", (req, res) => {
    finnhubClient.quote(req.query.symbol, (error, data, response) => {
        // console.log(data)
        res.send(data);
    });
})

app.get("/companyProfile", (req, res) => {
    finnhubClient.companyProfile({ 'symbol': req.query.symbol }, (error, data, response) => {
        // console.log(data)
        res.send(data);
    });
})

app.get("/companyProfile2", (req, res) => {
    finnhubClient.companyProfile2({ 'symbol': req.query.symbol }, (error, data, response) => {
        // console.log(data)
        res.send(data);
    });
})

app.get("/candle", (req, res) => {
    var today = new Date().getTime();
    var minus_5 = new Date();
    minus_5.setDate(minus_5.getDate() - 10);
    console.log(minus_5.getTime())
    console.log(today);
    finnhubClient.stockCandles(req.query.symbol, "D", parseInt(minus_5.getTime() / 1000), parseInt(today / 1000), (error, data, response) => {
        // console.log(data)
        res.send(data);
    });
})

app.get("/categoryMarketNews", (req, res) => {
    finnhubClient.marketNews(req.query.category, {}, (error, data, response) => {
        // console.log(data)
        res.send(data);
    });
})

app.get("/companyNews", (req, res) => {
    finnhubClient.companyNews(req.query.symbol, req.query.minus10Days, req.query.today, (error, data, response) => {
        // console.log(data)
        res.send(data);
    });
})

app.get("/dividends", (req, res) => {
    finnhubClient.stockBasicDividends(req.query.symbol, (error, data, response) => {
        console.log(data)
        res.send(data);
    });
})

app.get("/basicFinancials", (req, res) => {
    finnhubClient.companyBasicFinancials(req.query.symbol, "all", (error, data, response) => {
        console.log(data)
        res.send(data);
    });
})

app.get("/stockSymbol", (req, res) => {
    finnhubClient.stockSymbols("US", (error, data, response) => {
        console.log(data)
        res.send(data)
    });
})


app.get("/stocksList", (req, res) => {
    finnhubClient.symbolSearch(req.query.symbol, (error, data, response) => {
        console.log(data)
        res.send(data)
    });
})


//db
const db = require("./db/index");
app.use("/api/v1/user", require("./routes/user"));

db.authenticate()
  .then(() => console.log("Database connected..."))
  .catch((err) => console.log(err));

// Sync DBs
db.sync()
  .then(() => console.log("Models have been synced..."))
  .catch((err) => console.log(err));

  app.get('*', (req, res) => {
    res.sendFile(path.resolve(__dirname, 'client', 'build', 'index.html'));
});

const PORT = process.env.PORT || 8080;

app.listen(PORT, console.log(`Server started on port ${PORT}`));